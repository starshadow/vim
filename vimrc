execute pathogen#infect()
execute pathogen#helptags()

syntax enable

filetype plugin indent on

let g:airline_powerline_fonts = 1

set background=dark
set backspace=indent,eol,start
set directory=$HOME/.vim/tmp//
set expandtab
set fileformats=unix,dos,mac
set hlsearch
set incsearch
set laststatus=2
set list
set listchars=tab:>-,trail:-
set modeline
set number
set report=0
set ruler
set shiftround " when at 3 spaces, and I hit > ... go to 4, not 5
set showcmd
set t_Co=256
set textwidth=0
set wrapmargin=0

color dracula
